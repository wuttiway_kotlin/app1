package com.example.app1

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.example.app1.databinding.ActivityHelloBinding
import com.example.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.switchButton.setOnClickListener{
            Log.d(TAG, " "+binding.textviewName.text)
            Log.d(TAG, " "+binding.textviewId.text)
            onClick()
        }

    }
    fun onClick(){
        val intent = Intent(this, activity_hello::class.java)
        intent.putExtra("key",binding.textviewName.text)
        startActivity(intent)
    }

}