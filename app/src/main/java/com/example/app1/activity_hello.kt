package com.example.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import com.example.app1.databinding.ActivityHelloBinding

class activity_hello : AppCompatActivity() {
    private lateinit var binding: ActivityHelloBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        binding = ActivityHelloBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.sendTextview.text = intent.getStringExtra("key")
    }
}